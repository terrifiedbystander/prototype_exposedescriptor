package exposedescriptor.rest;

import javax.xml.bind.annotation.*;
@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExposeDescriptorRestModel {

    @XmlElement(name = "value")
    private String message;
    CustomUpdateValues custVal = new CustomUpdateValues();

    // file reader resource
    FileReaderResource fileReader = new FileReaderResource();


    public ExposeDescriptorRestModel() {
    }

    // read the json file
    public String readJsonFile() {

        return this.message = fileReader.readResource(custVal.getJsonFileLocation());

    }


    // read the html file
    public String readHtmlFile() {

        HtmlJCloud buildHtml = new HtmlJCloud();
        String fileObject = fileReader.readResource(custVal.getHtmlFileLocation());
        String newHTML = buildHtml.getBuildHTML(fileObject, custVal.getJsonLinkValue());

        return this.message = newHTML;
    }


    // get the raw JSON values from CustomUpdateValues
    public String getJsonValues() {

        return this.message = custVal.getOutputValues();
    }

}