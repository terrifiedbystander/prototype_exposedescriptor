package exposedescriptor.rest;

public class CustomUpdateValues {

    // file locations for exposing files
    private String jsonFileLocation = "JCloudExposure/atlassian-connect.json"; //json file location
    private String htmlFileLocation = "JCloudExposure/html-output.html"; //html file location

    // update the location of the output value of the json data - doesn't have to be Jira Server
    private String jsonLinkValue = "update link to json data location";

    /* json demonstration output data - ideally another plugin would pass the data to populate this. It is currently
       written in the form of JSON directly for ease of output.
    */
    private String outputValues = "{\"id\": \"0958\",\"content\": \"Content data output.\"}";


    private String setOutputValues(String string) {
        this.outputValues = string;
        return null;
    }

    public Void setJsonFileLocation(String string) {
        this.jsonFileLocation = string;
        return null;
    }

    public Void setHtmlFileLocation(String string) {
        this.htmlFileLocation = string;
        return null;
    }

    public Void setJsonLinkValue(String string) {
        this.jsonLinkValue = string;
        return null;
    }

    public String getJsonFileLocation() {
        return jsonFileLocation;
    }

    public String getHtmlFileLocation() {
        return htmlFileLocation;
    }


    public String getJsonLinkValue() {
        return jsonLinkValue;
    }

    public String getOutputValues(){
        return outputValues;
    }

}
