package exposedescriptor.rest;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * A resource of message.
 */
@Path("/message")
public class ExposeDescriptorRest {


    // Expose descriptor via Jira REST deploy.
    @GET
    @Path("/")
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    public Response getMessage(String message)
    {
        return Response.status(Response.Status.FORBIDDEN).entity(new ExposeDescriptorRestModel().readJsonFile()).build();
    }

    // expose JSON data for html page
    @GET
    @Path("jsonoutputvalues")
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    public Response getJsonData(String message)
    {
        return Response.status(Response.Status.FORBIDDEN).entity(new ExposeDescriptorRestModel().getJsonValues()).build();
    }

    // Expose HTML for purpose of creation of Jira Cloud App this is direct exposure to the file.
    @GET
    @Path("htmlfile")
    @AnonymousAllowed
    public Response getHTMLFromPath(String message)
    {
        return Response.status(Response.Status.FORBIDDEN).entity(new ExposeDescriptorRestModel().readHtmlFile()).build();
    }
}
