package exposedescriptor.rest;

public class HtmlJCloud {

    /*
        Updates certain values of the Jira Cloud default HTML. Simple but effective.
        This is needed as the HTML will change with updated values. Where as the JSON stays static.
     */



    public String getBuildHTML(String fileObject, String jsonLinkValue)  {

       /*
            // example of updating additional values in the HTML output

            htmlString = htmlString
                    .replaceAll("(?<=<h1>)(.*)(?=<\\/h1>)", updateValue) // update output value on header
                    .replaceAll("(?<=href=\")(.*)(?=\")", htmlHrefValue); // update href value
        */

        String htmlString = fileObject
                // update link for the json data to read
                .replaceAll("(?<=url: \")(.*)(?=\")", jsonLinkValue);

        return htmlString;
    }

}
