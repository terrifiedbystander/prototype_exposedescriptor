package exposedescriptor.rest;

import org.apache.commons.io.IOUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


public class FileReaderResource {
    public String readResource(String fileLocation){

        //String fileLocation = "JCloudExposure/test.json";

        String resource = null; // empty resource

        // Capture file and convert to UTF-8
        try {
            InputStream is = getClass().getClassLoader().getResourceAsStream(fileLocation);
            String fileTxt = IOUtils.toString(is, "UTF-8");
            resource = fileTxt;
        }
        catch(FileNotFoundException FNF){System.out.println(FNF);} // file not found err
        catch(IOException IOe){System.out.println(IOe);} // read file err

        return resource;
    }
}
